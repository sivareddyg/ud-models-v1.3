Loading default properties from tagger ud-models-v1.3/en/pos-tagger/utb-caseless-en-bidirectional-glove-distsim-lower.tagger
Reading POS tagger model from ud-models-v1.3/en/pos-tagger/utb-caseless-en-bidirectional-glove-distsim-lower.tagger ... done [0.5 sec].
Tagged 25096 words at 12765.01 words per second.
Model ud-models-v1.3/en/pos-tagger/utb-caseless-en-bidirectional-glove-distsim-lower.tagger has xSize=199919, ySize=18, and numFeatures=192002.
Results on 2077 sentences and 25096 words, of which 1878 were unknown.
Total sentences right: 1230 (59.220029%); wrong: 847 (40.779971%).
Total tags right: 23416 (93.305706%); wrong: 1680 (6.694294%).
Unknown words right: 1449 (77.156550%); wrong: 429 (22.843450%).

